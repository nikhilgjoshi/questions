# Questions
Question 1
Choosing a Scalable Solution for Periodic Task Scheduling
When it comes to scheduling periodic tasks, such as downloading a list of ISINs every 24 hours, a reliable and scalable system is crucial to ensure seamless operation and efficient performance. After careful consideration, we opted for the Celery distributed task queue in our production environment.

Choice of Celery:
Celery is a widely-used, battle-tested distributed task queue that integrates seamlessly with Python, making it an ideal choice for our application. Its robustness, versatility, and community support were the key factors behind our decision. With Celery, we can easily define tasks, distribute them across multiple workers, and execute them asynchronously.

Reliability:
Celery's fault-tolerant design and support for task retries ensure that even if a worker fails, tasks are not lost, and they can be re-executed once the issue is resolved. This reliability factor is crucial for critical periodic tasks like updating ISINs, as we can't afford data inconsistencies or data loss.

Scalability:
Celery's distributed architecture allows us to scale our task processing capacity horizontally by adding more worker nodes as our workload grows. This horizontal scalability ensures smooth task execution, even during peak periods, and enables us to handle increasing volumes of ISINs without compromising performance.

Potential Challenges:
While Celery is an excellent choice for our current needs, there are some considerations to keep in mind. Managing large task queues and worker nodes might require careful resource allocation and monitoring to avoid bottlenecks. Additionally, complex task dependencies and handling of long-running tasks might require further optimization.

Recommendation for Production Scale: To address potential scaling challenges in production, we recommend the following measures:

Load Balancing: Implementing load balancing mechanisms to distribute tasks evenly among worker nodes will prevent overloading and optimize task execution.

Task Prioritization: Assigning priorities to tasks can help ensure that critical tasks, such as ISIN updates, are processed promptly.

Task Result Backend: Configuring a robust result backend, like Redis or RabbitMQ, ensures task results are efficiently stored and retrieved.

Task Monitoring: Utilizing monitoring tools and dashboards will help track task performance, identify bottlenecks, and maintain overall system health.

Auto-scaling: Implementing auto-scaling mechanisms can dynamically adjust the number of worker nodes based on demand, reducing operational overhead and optimizing resource utilization.

By proactively addressing these considerations and leveraging Celery's strengths, we are confident in our ability to reliably and efficiently schedule periodic tasks, scale our application, and deliver a seamless user experience in production.

Question 2
To achieve secure data encryption and ensure that even the developer cannot view the sensitive bank statements, we can adopt a combination of modern encryption techniques and a zero-knowledge architecture. Here's an outline of the steps we would take to solve this problem:

Client-Side Encryption: Implement client-side encryption in the financial planning tool. When the user uploads their bank statements, the data should be encrypted on the user's device using strong encryption algorithms and keys.

Zero-Knowledge Encryption: Adopt a zero-knowledge architecture to ensure that the server or any other party, including the developers, have no access to the user's encryption keys or decrypted data. The encryption and decryption processes must be entirely handled on the user's device without any involvement from the server.

Secure Key Generation: Use a secure key generation mechanism on the client-side to create encryption keys. These keys should be generated using robust cryptographic libraries and techniques. Avoid storing or transmitting encryption keys to the server or any external location.

Secure Data Transmission: Utilize secure communication protocols (e.g., HTTPS) to transmit the encrypted bank statements from the client's device to the server. This ensures that the encrypted data remains confidential during transmission.

Secure Data Storage: Store only the encrypted bank statements on the server. The server should have no visibility into the content or decryption keys of the data. The data should be stored in encrypted form using state-of-the-art encryption algorithms.

Key Management: Implement a secure key management system that adheres to industry best practices. Safeguard encryption keys and avoid hardcoding them in the codebase or storing them in plain text.

Multi-Factor Authentication (MFA): Enforce multi-factor authentication to enhance the security of user accounts. This helps prevent unauthorized access to the user's encrypted data.

Regular Security Audits: Conduct regular security audits and vulnerability assessments to identify and address any potential weaknesses or security gaps in the system.

Data Deletion: Offer users the option to permanently delete their encrypted bank statements from the server whenever they wish.

By implementing these measures, we can ensure that bank statements are encrypted securely, and only the users themselves have access to their decrypted data. This approach provides a high level of data privacy and confidentiality, making the financial planning tool trustworthy and reliable for users to safeguard their sensitive information.
